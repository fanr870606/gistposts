import React, { Component } from "react";
import {
  Container,
  Content
} from "native-base";
import GistDetail from "../../components/gistDetail/gistDetail";

class GistDetailScreen extends Component {
  render() {
    return (
      <Container>
        <Content padder>
          <GistDetail gist={this.props.gist} />
        </Content>
      </Container>
    );
  }
}

export default GistDetailScreen;
