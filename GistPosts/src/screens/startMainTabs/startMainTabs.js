import { Platform } from "react-native";
import { Navigation } from "react-native-navigation";
import Icon from "react-native-vector-icons/Ionicons";
import screens from "../../strings/screens";
import text from "../../strings/text";


startMainTabs = () => {
    Promise.all([
        Icon.getImageSource(Platform.OS === "android" ? "md-book" : "ios-book", 24),
        Icon.getImageSource(Platform.OS === "android" ? "md-create" : "ios-create", 24),
        Icon.getImageSource(Platform.OS === "android" ? "md-menu" : "ios-menu", 24),
    ]).then(sources => {
        Navigation.startTabBasedApp({
            tabs: [
                {
                    screen: screens.readerScreen,
                    label: text.tab_readerlabel,
                    title: text.tab_readertitle,
                    icon: sources[0],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                },
                {
                    screen: screens.writerScreen,
                    label: text.tab_writerlabel,
                    title: text.tab_writertitle,
                    icon: sources[1],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                }
            ],
            tabsStyle: {
                tabBarSelectedButtonColor: "green"
            },
            appStyle: {
                tabBarSelectedButtonColor: "green"
            },
            drawer: {
                left: {
                    screen: screens.sideDrawerScreen
                }
            }
        });
    });
};

export default startMainTabs;
