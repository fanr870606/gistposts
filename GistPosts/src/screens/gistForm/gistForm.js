import React, { Component } from "react";
import { Text, AsyncStorage } from "react-native";
import {
  Container,
  Content
} from "native-base";
import GistForm from "../../components/gistForm/gistForm";

class GistFormScreen extends Component {
  render() {
    return (
      <Container>
        <Content padder>
          <GistForm {...this.props} gist={this.props.gist} editMode={this.props.editMode} />
        </Content>
      </Container>
    );
  }
}

export default GistFormScreen;
