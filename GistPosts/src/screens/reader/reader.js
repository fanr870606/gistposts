import React, { Component } from "react";
import { View, FlatList, TouchableOpacity, AsyncStorage } from "react-native";
import {
    Container,
    Header,
    Item,
    Input,
    Icon,
    Content,
    Spinner
} from "native-base";
import GistExcerpt from "../../components/gistExcerpt/gistExcerpt";
import urls from "../../strings/url";
import values from "../../strings/values";
import texts from "../../strings/text";
import screens from "../../strings/screens";
import { getReaderData, changeFilterUsername, changeFilterMode, removeFilterMode } from "../../store/actions/index";
import { connect } from "react-redux";

class ReaderScreen extends Component {

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress") {
            if (event.id === "sideDrawerToggle") {
                this.props.navigator.toggleDrawer({
                    side: "left"
                });
            }
        }
    };

    componentDidMount() {
        this.props.getReaderData();
    }

    _refresh() {
        this.props.getReaderData();
    }

    _onFilterTextChange = filterText => {
        this.props.changeFilterUsername(filterText);
    };

    _onFilterClick = () => {
        this.props.changeFilterMode();
    };

    _onDeleteFilterClick = () => {
        if (this.props.filterActivate) {
            this.props.removeFilterMode();
        }
    };

    _keyExtractor = (item, index) => item.id;

    _detailGist = item => {
        this.props.navigator.push({
            screen: screens.detailScreen,
            title: texts.tab_readerdetail,
            passProps: { gist: item }
        });
    };

    _renderItem = ({ item }) => (
        <GistExcerpt item={item} onItemPress={() => this._detailGist(item)} />
    );

    render() {

        let content = (<FlatList
            data={this.props.readerData}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
        />);

        if (this.props.isLoading) {
            content = (<Spinner color='green' />);
        }

        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <TouchableOpacity onPress={this._onFilterClick}>
                            <Icon
                                style={{
                                    color: this.props.filterActivate
                                        ? values.color_filter_activate_search
                                        : null
                                }}
                                ios="ios-search"
                                android="md-search"
                            />
                        </TouchableOpacity>
                        <Input
                            placeholder={texts.reader_search_label}
                            value={this.props.filterUsername}
                            onChangeText={filterText => this.props.changeFilterUsername(filterText)}
                        />
                        <TouchableOpacity onPress={this._onDeleteFilterClick}>
                            <Icon
                                style={{
                                    color: this.props.filterActivate
                                        ? values.color_filter_activate_delete
                                        : null
                                }}
                                ios="ios-close-circle"
                                android="md-close-circle"
                            />
                        </TouchableOpacity>
                    </Item>
                </Header>
                <Content>
                    {content}
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.gist.isReaderLoading,
        readerData: state.gist.readerData,
        filterActivate: state.gist.filterActivate,
        filterUsername: state.gist.filterUsername
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getReaderData: () =>
            dispatch(getReaderData()),
        changeFilterUsername: (filterUsername) =>
            dispatch(changeFilterUsername(filterUsername)),
        changeFilterMode: () => dispatch(changeFilterMode()),
        removeFilterMode: () => dispatch(removeFilterMode())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReaderScreen);
