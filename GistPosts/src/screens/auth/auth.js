import React, { Component } from "react";
import { AsyncStorage } from "react-native";
import {
  Container,
  Header,
  Content,
  Form,
  Button,
  Text,
  Icon,
  Spinner
} from "native-base";
import { authorize } from "react-native-app-auth";
import authorizeValues from "../../strings/values";
import startMainTabs from "../../screens/startMainTabs/startMainTabs";
import { authAutoSignIn, tryAuth, uiStartLoading, uiStopLoading } from "../../store/actions/index";
import { connect } from "react-redux";

class AuthScreen extends Component {

  componentWillMount() {
    this.props.onAutoSignIn();
  }

  github = async () => {
    const config = {
      serviceConfiguration: {
        authorizationEndpoint: authorizeValues.auth_authorizationEndpoint,
        tokenEndpoint: authorizeValues.auth_tokenEndpoint
      },
      clientId: authorizeValues.auth_clientId,
      clientSecret: authorizeValues.auth_clientSecret,
      redirectUrl: authorizeValues.auth_redirectUrl,
      scopes: [authorizeValues.auth_gistScope]
    };

    try {
      this.props.uiStartLoading();      
      const result = await authorize(config);
      this.props.tryAuth(result.accessToken);
    } catch (error) {
      this.props.uiStopLoading();
      alert(error);
    }
  };

  render() {

    if (this.props.isLoading) {
      return (<Spinner color="green" />);
    }

    return (
      <Container>
        <Content>
          <Form>
            <Icon name="logo-github" style={{ fontSize: 140, alignSelf: 'center', }} />
            <Button block success onPress={this.github}>
              <Text>Sign In</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAutoSignIn: () => dispatch(authAutoSignIn()),
    tryAuth: (token) => dispatch(tryAuth(token)),
    uiStartLoading: () => dispatch(uiStartLoading()),
    uiStopLoading: () => dispatch(uiStopLoading())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthScreen);
