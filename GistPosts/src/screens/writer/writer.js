import React, { Component } from "react";
import {
    View,
    FlatList,
    AsyncStorage,
} from "react-native";
import { Fab, Icon, Spinner } from "native-base";
import screens from "../../strings/screens";
import GistExcerpt from "../../components/gistExcerpt/gistExcerpt";
import texts from "../../strings/text";
import { getWriterData } from "../../store/actions/index";
import { connect } from "react-redux";

class WriterScreen extends Component {
    state = {
        active: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress") {
            if (event.id === "sideDrawerToggle") {
                this.props.navigator.toggleDrawer({
                    side: "left"
                });
            }
        }
    };

    componentWillMount() {
        this.props.getWriterData();
    }

    _createNew = () => {
        this.setState({ active: !this.state.active });
        this.props.navigator.push({
            screen: screens.formScreen,
            title: texts.tab_writernew,
            passProps: { gist: null, editMode: false }
        });
    };

    _editGist = item => {
        this.props.navigator.push({
            screen: screens.formScreen,
            title: texts.tab_writeredit,
            passProps: { gist: item, editMode: true }
        });
    };

    _keyExtractor = (item, index) => item.id;

    _renderItem = ({ item }) => <GistExcerpt item={item} onItemPress={() => this._editGist(item)} />;

    render() {
        let content = (<FlatList
            data={this.props.writerData}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
        />);

        if (this.props.isLoading) {
            content = (<Spinner color='green' />);
        }

        return (
            <View style={{ flex: 1 }}>
                {content}
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{}}
                    style={{ backgroundColor: "red" }}
                    position="bottomRight"
                    onPress={this._createNew}
                >
                    <Icon ios="ios-add" android="md-add" />
                </Fab>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.gist.isWriterLoading,
        writerData: state.gist.writerData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getWriterData: () =>
            dispatch(getWriterData())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WriterScreen);
