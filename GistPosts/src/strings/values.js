module.exports = {
    auth_authorizationEndpoint: "https://github.com/login/oauth/authorize",
    auth_tokenEndpoint: "https://github.com/login/oauth/access_token",
    auth_clientId: "d01d85bd5c1ddac2dfc0",
    auth_clientSecret: "95a0df0fef83717119f6af8e7b9dc4bb5f35f8d8",
    auth_redirectUrl: "gistposts://oauth",
    auth_gistScope: "gist",
    color_public_post: "green",
    color_private_post: "red",
    color_filter_activate_search: "green",
    color_filter_activate_delete: "red",
};