module.exports = {
    authScreen: "gistpost.auth",
    readerScreen: "gistpost.reader",
    writerScreen: "gistpost.writer",
    formScreen: "gistpost.form",
    detailScreen: "gistpost.detail",
    sideDrawerScreen: "gistpost.drawer",
};
