module.exports = {
    getAllGists: "https://api.github.com/gists/public",
    getGist: "https://api.github.com/gists/",
    getWriterGist: "https://api.github.com/gists?accesstoken=",
    getGistByUser: "https://api.github.com/users/:username/gists",
    createGist: "https://api.github.com/gists?accesstoken=",
    updateGist: "https://api.github.com/gists/",
    accesstokenParam: "?accesstoken=",
};
