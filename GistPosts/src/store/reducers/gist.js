import {
    GIST_SET_READER_DATA, GIST_SET_FILTER_MODE, GIST_CHANGE_FILTER_USERNAME, GIST_CHANGE_FILTER_MODE, GIST_SET_WRITER_DATA,
    GIST_START_READER_LOADING, GIST_STOP_READER_LOADING, GIST_START_WRITER_LOADING, GIST_STOP_WRITER_LOADING, GIST_START_FORM_LOADING,
    GIST_STOP_FORM_LOADING
} from "../actions/actionTypes";

const initialState = {
    readerData: [],
    writerData: [],
    filterActivate: false,
    filterUsername: "",
    isReaderLoading: false,
    isWriterLoading: false,
    isFormLoading: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GIST_SET_READER_DATA:
            return {
                ...state,
                readerData: action.readerData
            };
            break;
        case GIST_SET_WRITER_DATA:
            return {
                ...state,
                writerData: action.writerData
            };
            break;
        case GIST_SET_FILTER_MODE:
            return {
                ...state,
                filterActivate: action.filterActivate
            };
        case GIST_CHANGE_FILTER_MODE:
            return {
                ...state,
                filterActivate: !state.filterActivate
            };
        case GIST_CHANGE_FILTER_USERNAME:
            return {
                ...state,
                filterUsername: action.filterUsername
            };
        case GIST_START_READER_LOADING:
            return {
                ...state,
                isReaderLoading: true
            };
        case GIST_STOP_READER_LOADING:
            return {
                ...state,
                isReaderLoading: false
            };
        case GIST_START_WRITER_LOADING:
            return {
                ...state,
                isWriterLoading: true
            };
        case GIST_STOP_WRITER_LOADING:
            return {
                ...state,
                isWriterLoading: false
            };
        case GIST_START_FORM_LOADING:
            return {
                ...state,
                isFormLoading: true
            };
        case GIST_STOP_FORM_LOADING:
            return {
                ...state,
                isFormLoading: false
            };
        default:
            return state;
    }
};

export default reducer;
