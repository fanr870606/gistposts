import { Navigation } from "react-native-navigation";
import { AUTH_REMOVE_TOKEN, AUTH_SET_TOKEN } from "./actionTypes";
import { AsyncStorage } from "react-native";
import StartMainTabs from "../../screens/startMainTabs/startMainTabs"
import storageKeys from "../../strings/storageKeys"
import { uiStartLoading, uiStopLoading } from "./index";
import screens from "../../strings/screens";
import text from "../../strings/text";
import startMainTabs from "../../screens/startMainTabs/startMainTabs";


export const authGetToken = () => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            let token = getState().auth.token;
            if (!token) {
                AsyncStorage.getItem(storageKeys.authTokenKey)
                    .then(tokenFromStorage => {
                        if (!tokenFromStorage) {
                            reject();
                            return;
                        }
                        resolve(tokenFromStorage);
                    })
                    .catch(error => reject())
                    ;
            } else {
                resolve(token);
            }
        })
    }
};

export const authAutoSignIn = () => {
    return dispatch => {
        dispatch(uiStartLoading());
        dispatch(authGetToken())
            .then(token => {
                StartMainTabs();
                dispatch(uiStopLoading());
            })
            .catch(error => {
                dispatch(uiStopLoading());
                console.log(error);
            });
    };
};

export const authLogout = () => {
    return dispatch => {
        dispatch(authRemoveToken);
        AsyncStorage.removeItem(storageKeys.authTokenKey)
            .then(() => {
                Navigation.startSingleScreenApp({
                    screen: {
                        screen: screens.authScreen,
                        title: text.auth_login
                    }
                });
            });
    };
};

export const tryAuth = (token) => {
    return dispatch => {
        dispatch(setToken(token));
        AsyncStorage.setItem(storageKeys.authTokenKey, token)
            .then(() => { 
                dispatch(uiStopLoading());
                StartMainTabs();
             });
    };
};

export const authRemoveToken = () => {
    return {
        type: AUTH_REMOVE_TOKEN
    };
};

export const setToken = (token) => {
    return {
        type: AUTH_SET_TOKEN,
        token: token
    };
};