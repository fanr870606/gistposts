import {
    GIST_SET_READER_DATA, GIST_CHANGE_FILTER_USERNAME, GIST_SET_FILTER_MODE, GIST_CHANGE_FILTER_MODE, GIST_SET_WRITER_DATA,
    GIST_START_READER_LOADING, GIST_STOP_READER_LOADING, GIST_START_WRITER_LOADING, GIST_STOP_WRITER_LOADING, GIST_START_FORM_LOADING,
    GIST_STOP_FORM_LOADING
} from "./actionTypes";
import { authGetToken } from "../../store/actions/index";
import urls from "../../strings/url";
import texts from "../../strings/text";

export const getReaderData = () => {
    return (dispatch, getState) => {
        dispatch(startReaderLoading());
        let url = urls.getAllGists;

        if (getState().gist.filterActivate) {
            url = urls.getGistByUser.replace(":username", getState().gist.filterUsername);
        }

        dispatch(authGetToken()).
            then(token => {
                return fetch(url + urls.accesstokenParam + token, {
                    headers: new Headers({
                        Authorization: "token " + token,
                        "Content-Type": "application/x-www-form-urlencoded"
                    })
                });
            })
            .then(resp => resp.json())
            .then(jsonResp => {
                dispatch(stopReaderLoading());
                dispatch(setReaderData(jsonResp));
            })
            .catch(error => {
                dispatch(stopReaderLoading());
                alert("error");
            });;
    };
};

export const changeFilterMode = () => {
    return (dispatch, getState) => {
        dispatch(setFilterMode(!getState().gist.filterActivate));
        dispatch(getReaderData());
    };
};

export const removeFilterMode = () => {
    return (dispatch, getState) => {
        dispatch(setFilterMode(false));
        dispatch(setFilterUsername(""));
        dispatch(getReaderData());
    };
};

export const setFilterMode = (filterActivate) => {
    return {
        type: GIST_SET_FILTER_MODE,
        filterActivate: filterActivate
    };
};

export const changeFilterUsername = (filterUsername) => {
    return (dispatch, getState) => {
        dispatch(setFilterUsername(filterUsername));
        if (getState().gist.filterActivate) {
            dispatch(getReaderData());
        }
    };
};

export const setFilterUsername = (filterUsername) => {
    return {
        type: GIST_CHANGE_FILTER_USERNAME,
        filterUsername: filterUsername
    };
};

export const setReaderData = (data) => {
    return {
        type: GIST_SET_READER_DATA,
        readerData: data
    };
};

export const startReaderLoading = () => {
    return {
        type: GIST_START_READER_LOADING
    };
};

export const stopReaderLoading = () => {
    return {
        type: GIST_STOP_READER_LOADING
    };
};

export const getWriterData = () => {
    return (dispatch, getState) => {
        dispatch(startWriterLoading());
        dispatch(authGetToken()).
            then(token => {
                return fetch(urls.getWriterGist + token, {
                    headers: new Headers({
                        Authorization: "token " + token,
                        "Content-Type": "application/x-www-form-urlencoded"
                    })
                });
            })
            .then(resp => resp.json())
            .then(jsonResp => {
                dispatch(setWriterData(jsonResp));
                dispatch(stopWriterLoading());
            })
            .catch(error => {
                dispatch(stopWriterLoading());
                alert("error");
            });;
    };
};



export const setWriterData = (data) => {
    return {
        type: GIST_SET_WRITER_DATA,
        writerData: data
    };
};

export const startWriterLoading = () => {
    return {
        type: GIST_START_WRITER_LOADING
    };
};

export const stopWriterLoading = () => {
    return {
        type: GIST_STOP_WRITER_LOADING
    };
};

export const createGist = (newGist, navigator) => {
    return (dispatch, getState) => {
        dispatch(startFormLoading());
        dispatch(authGetToken()).
            then(token => {
                return fetch(urls.createGist + token, {
                    method: "POST",
                    body: JSON.stringify(newGist),
                    headers: new Headers({
                        Authorization: "token " + token,
                        "Content-Type": "application/x-www-form-urlencoded"
                    })
                })
            })
            .then(resp => resp.json())
            .then(jsonResp => {
                dispatch(getWriterData());                                
                dispatch(stopFormLoading());
                navigator.pop();
                alert(texts.writer_create_success);         
            })
            .catch(error => {
                dispatch(stopFormLoading());
                alert(error);
            });
    };
};

export const updateGist = (gistId, modifyGist, navigator) => {
    return (dispatch, getState) => {
        dispatch(startFormLoading());
        dispatch(authGetToken()).
            then(token => {
                return fetch(`${urls.updateGist}${gistId}${urls.accesstokenParam}${token}`, {
                    method: "POST",
                    body: JSON.stringify(modifyGist),
                    headers: new Headers({
                        Authorization: "token " + token,
                        "Content-Type": "application/x-www-form-urlencoded"
                    })
                })
            })
            .then(resp => resp.json())
            .then(jsonResp => {
                dispatch(getWriterData());                
                dispatch(stopFormLoading());
                navigator.pop();
                alert(texts.writer_update_success);
            })
            .catch(error => {
                dispatch(stopFormLoading());
                alert(error);
            });
    };
};


export const startFormLoading = () => {
    return {
        type: GIST_START_FORM_LOADING
    };
};

export const stopFormLoading = () => {
    return {
        type: GIST_STOP_FORM_LOADING
    };
};