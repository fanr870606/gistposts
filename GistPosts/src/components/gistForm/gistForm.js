import React, { Component } from "react";
import { Text, AsyncStorage, ActivityIndicator } from "react-native";
import {
    Textarea,
    Item,
    Input,
    Icon,
    Form,
    Button,
    ListItem,
    CheckBox,
    Body,
    Spinner
} from "native-base";
import texts from "../../strings/text";
import values from "../../strings/values";
import urls from "../../strings/url";
import styles from "../../styles/gistForm.styles";
import { createGist, updateGist } from "../../store/actions/index";
import { connect } from "react-redux";

class GistForm extends Component {
    state = {
        gistDescription: "",
        filename: texts.writer_default_filename,
        previousFilename: "",
        content: "",
        public: true
    };

    constructor(props) {
        super(props);
        if (props.gist) {
            this.loadGistInformation(props.gist.id);
        }
    }

    loadGistInformation(gistId) {
        AsyncStorage.getItem("token").then(token => {
            fetch(urls.getGist + gistId, {
                method: "GET",
                headers: new Headers({
                    Authorization: "token " + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                })
            })
                .then(resp => resp.json())
                .then(jsonResp => {
                    const fileName = Object.keys(jsonResp.files)[0];
                    const fileObject = jsonResp.files[fileName];
                    this.setState({ gistDescription: jsonResp.description });
                    this.setState({ filename: fileName });
                    this.setState({ previousFilename: fileName });
                    this.setState({ content: fileObject.content });
                })
                .catch(error => {
                    alert(error);
                });
        });
    }

    _onCheckboxClick = () => {
        this.setState(prevState => ({
            public: !prevState.public
        }));
    };

    _create = () => {
        const newGist = {
            description: this.state.gistDescription,
            public: this.state.public,
            files: {}
        };

        newGist.files[this.state.filename] = {
            content: this.state.content
        };

        this.props.createGist(newGist, this.props.navigator);
    };

    _edit = () => {
        const modifyGist = {
            description: this.state.gistDescription,
            public: this.state.public,
            files: {}
        };

        modifyGist.files[this.state.filename] = {
            content: this.state.content
        };

        if (this.state.filename != this.state.previousFilename) {
            modifyGist.files[this.state.previousFilename] = null;
        }

        this.props.updateGist(this.props.gist.id, modifyGist, this.props.navigator);
    };

    render() {

        let buttonContent = (<Button
            block
            success={!this.props.editMode}
            danger={this.props.editMode}
            onPress={this.props.editMode ? this._edit : this._create}
        >
            <Icon
                ios={this.props.editMode ? "ios-sync" : "ios-create"}
                android={this.props.editMode ? "md-sync" : "md-create"}
            />
            <Text style={styles.textButton}>
                {this.props.editMode
                    ? texts.writer_button_update
                    : texts.writer_button_create}
            </Text>
        </Button>);

        if (this.props.isLoading) {
            buttonContent = (<Spinner color='green' />);
        }

        let publicGist = null;

        if (!this.props.editMode) {
            publicGist = (<ListItem>
                <CheckBox
                    checked={this.state.public}
                    onPress={this._onCheckboxClick}
                    color={this.state.public ? values.color_public_post : values.color_private_post}
                />
                <Body>
                    <Text style={[styles.labelCheckBox, this.state.public ? styles.labelPublicPost : styles.labelPrivatePost]}>
                        {this.state.public ? texts.writer_checkbox_label_public : texts.writer_checkbox_label_private}
                    </Text>
                </Body>
            </ListItem>);
        };

        return (
            <Form>
                <Item>
                    <Input
                        placeholder={texts.writer_filename_placeholder}
                        value={this.state.filename}
                        onChangeText={filename => this.setState({ filename })}
                    />
                    <Icon active ios="ios-paper" android="md-paper" />
                </Item>
                <Item>
                    <Input
                        placeholder={texts.writer_description_placeholder}
                        value={this.state.gistDescription}
                        onChangeText={gistDescription => this.setState({ gistDescription })}
                    />
                    <Icon active ios="ios-document" android="md-document" />
                </Item>
                <Textarea
                    rowSpan={8}
                    bordered
                    placeholder={texts.writer_filecontent_placeholder}
                    value={this.state.content}
                    onChangeText={content => this.setState({ content })}
                />
                {publicGist}
                {buttonContent}
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        isLoading: state.gist.isFormLoading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createGist: (newGist, navigator) =>
            dispatch(createGist(newGist, navigator)),
        updateGist: (id, modifyGist, navigator) =>
            dispatch(updateGist(id, modifyGist, navigator))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GistForm);