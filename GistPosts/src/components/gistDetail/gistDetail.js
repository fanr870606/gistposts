import React, { Component } from "react";
import { Text, ScrollView, AsyncStorage } from "react-native";
import {
    Card,
    CardItem,
    Left,
    Body,
    Thumbnail
} from "native-base";

import texts from "../../strings/text";
import urls from "../../strings/url";

class GistDetail extends Component {
    state = {
        gistDescription: "",
        filename: "",
        previousFilename: "",
        content: "",
        public: true
    };

    constructor(props) {
        super(props);
        if (props.gist) {
            this.loadGistInformation(props.gist.id);
        }
    }

    loadGistInformation(gistId) {
        AsyncStorage.getItem("token").then(token => {
            fetch(urls.getGist + gistId, {
                method: "GET",
                headers: new Headers({
                    Authorization: "token " + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                })
            })
                .then(resp => resp.json())
                .then(jsonResp => {
                    const fileName = Object.keys(jsonResp.files)[0];
                    const fileObject = jsonResp.files[fileName];
                    this.setState({ gistDescription: jsonResp.description });
                    this.setState({ filename: fileName });
                    this.setState({ previousFilename: fileName });
                    this.setState({ content: fileObject.content });
                })
                .catch(error => {
                    alert(error);
                });
        });
    }

    render() {
        return (
            <Card style={{ flex: 0 }}>
                <CardItem>
                    <Thumbnail source={{ uri: this.props.gist.owner.avatar_url }} defaultSource={require("../../assets/profile.png")} />
                    <Body>
                        <Text>{this.props.gist.owner.login}</Text>
                        <Text>{Object.keys(this.props.gist.files)[0]}</Text>
                        <Text note>{texts.reader_created_at} {this.props.gist.created_at}</Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body>
                        <Text>
                            {this.state.content}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}

export default GistDetail;
