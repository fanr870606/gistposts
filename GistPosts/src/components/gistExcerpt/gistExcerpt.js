import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Icon, Container } from "native-base";

import text from "../../strings/text";
import styles from "../../styles/gistExcerpt.styles";

class GistExcerpt extends Component {

  render() {
    return (
      <TouchableOpacity style={styles.gistMainContainer} onPress={this.props.onItemPress}>
        <View style={styles.gistImageContainer}>
          <Image style={styles.avatarImage} source={{ uri: this.props.item.owner.avatar_url }} defaultSource={require("../../assets/profile.png")} />
        </View>
        <View style={styles.gistGeneralContentContainer}>
          <View style={styles.gistTextsContainer}>
            <Text style={styles.userText}> {this.props.item.owner.login} </Text>
            <Text> {Object.keys(this.props.item.files)[0]} </Text>
            <Text> {text.reader_created_at} {this.props.item.created_at} </Text>
          </View>
          <View styles={styles.gistIconsContainer}>
            <Icon ios='ios-chatbubbles' android="md-chatbubbles" />
            <Text> {this.props.item.comments} </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default GistExcerpt;
