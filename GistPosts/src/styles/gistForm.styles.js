import { StyleSheet, Dimensions, Platform } from 'react-native';

export default StyleSheet.create({
    textButton: {
        color: "white"
    },
    labelCheckBox: {
        paddingLeft: 10,
        fontSize: 16,
        fontWeight: "bold"
    },
    labelPublicPost: {
        color: "green"
    },
    labelPrivatePost: {
        color: "red"
    },
    userText:{
        color: "blue",
        fontSize: 16,
    }
});