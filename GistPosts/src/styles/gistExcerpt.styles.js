import { StyleSheet, Dimensions, Platform } from 'react-native';

export default StyleSheet.create({
    gistMainContainer: {
        borderTopWidth: 0.4,
        borderBottomWidth: 0.4,
        borderColor: "grey",
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
    },
    gistImageContainer: {
        paddingHorizontal: 10,
    },
    avatarImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    gistTextsContainer: {
        flexDirection: "column",
        justifyContent: "space-between"
    },
    gistGeneralContentContainer: {
        flexDirection: "column",
        justifyContent: "space-between"
    },
    gistIconsContainer: {
        flexDirection: "row"
    },
    userText:{
        color: "blue",
        fontSize: 18,
        fontWeight: 'bold',
    }
});