
import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";

import AuthScreen from "./src/screens/auth/auth";
import SideDrawerScreen from "./src/screens/sideDrawer/sideDrawer";
import ReaderScreen from "./src/screens/reader/reader";
import WriterScreen from "./src/screens/writer/writer";
import GistFormScreen from "./src/screens/gistForm/gistForm";
import GistDetailScreen from "./src/screens/gistDetail/gistDetail";

import configureStore from "./src/store/configureStore";
import screens from "./src/strings/screens";
import text from "./src/strings/text";

const store = configureStore();

// Register Screens
Navigation.registerComponent(
  screens.authScreen,
  () => AuthScreen,
  store,
  Provider
);

Navigation.registerComponent(
  screens.readerScreen,
  () => ReaderScreen,
  store,
  Provider
);

Navigation.registerComponent(
  screens.writerScreen,
  () => WriterScreen,
  store,
  Provider
);

Navigation.registerComponent(
  screens.formScreen,
  () => GistFormScreen,
  store,
  Provider
);

Navigation.registerComponent(
  screens.detailScreen,
  () => GistDetailScreen,
  store,
  Provider
);

Navigation.registerComponent(
  screens.sideDrawerScreen,
  () => SideDrawerScreen,
  store,
  Provider
);

// Start a App
Navigation.startSingleScreenApp({
  screen: {
    screen: screens.authScreen,
    title: text.auth_login
  }
});
